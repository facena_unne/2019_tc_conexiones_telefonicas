/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utiles;

import analisis.Comunicacion;
import entidades.Telefono;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */


public class Herramienta{
    //codigos de area.
    public static  Map<Integer, String> codigos = new HashMap<Integer, String>();
    
    /**
     * lee el archivo './codigos-de-area.txt' con los codigos de area y los paises.
     */
    public static void readCodigosDeArea (){
        try {            
            FileInputStream fis = new FileInputStream("./codigos-de-area.txt");
            InputStreamReader isr = new InputStreamReader(fis,"utf8");
            BufferedReader br = new BufferedReader(isr);

            String linea;
            while((linea = br.readLine()) != null){
                String[]_line = linea.split(",");
                int _codigo = Integer.parseInt(  _line[0] );
                codigos.put( _codigo, _line[1]);                
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } 
    }
    /**
     * lote de pruebas de la guia.          
     * @param p_nroLLamadas numero de llamadas a generar al lote 
     * @param p_imprimirLote  indica si es necesario imprimir en la consola el lote de pruba.     
     */
    public static ArrayList<Comunicacion> loteDePrueba(int p_nroLLamadas, boolean p_imprimirLote)
    {
        ArrayList<Comunicacion> comunicaciones = new ArrayList<Comunicacion>();
        
        
        //DATOS DE LA GUIA
        /*comunicaciones.add( new Comunicacion(new Telefono("0171 378 0647") ));  
        comunicaciones.add( new Comunicacion(new Telefono("(44.171)920 1007")));  
        comunicaciones.add( new Comunicacion(new Telefono("+44(0) 1225 753678")));  
        comunicaciones.add(new Comunicacion(new Telefono("01256 468551")));  
        comunicaciones.add(new Comunicacion(new Telefono("(96) 590-34-00")));//españa llamada local  
        comunicaciones.add(new Comunicacion(new Telefono("96 590 3400")));  //españa llamada local
        comunicaciones.add(new Comunicacion(new Telefono("1-845-225-3000")));  
        comunicaciones.add(new Comunicacion(new Telefono("212.995.5405"))); //USA llamada local 
        comunicaciones.add(new Comunicacion(new Telefono("+45 43 48 60 61")));  
        comunicaciones.add(new Comunicacion(new Telefono("95-51-279648")));  //pakistan llamada local
        
        comunicaciones.add(new Comunicacion(new Telefono("+411/285 3797")));  
        comunicaciones.add(new Comunicacion(new Telefono("+49 69 136-2 98 05")));  
        comunicaciones.add(new Comunicacion(new Telefono("33 1 34 43 32 26")));  
        comunicaciones.add(new Comunicacion(new Telefono("++31-20-5200161")));  */
        comunicaciones.add(new Comunicacion(new Telefono("+54 379-4465084")));  
        comunicaciones.add(new Comunicacion(new Telefono("+54 11 3514 3874")));          
        //GUIA
        
        if (p_nroLLamadas>0){            
            int millisInDay = 60*60*1000;    
            Random random = new Random();
            for (int i =1; i<=p_nroLLamadas;i++){
                Long _timeMillis = System.currentTimeMillis();            
                Comunicacion _comunicacion = new Comunicacion(
                    new Telefono(Herramienta.randomNro(12)),//numerod e relefono con longitud 12 y simbolo alea'+'
                    new Telefono(Herramienta.randomNro(12)),//numerod e relefono con longitud 12 y simbolo alea'+'
                    new Timestamp(_timeMillis),
                    new Timestamp(
                       _timeMillis + random.nextInt(millisInDay)
                    )
                );                    
                comunicaciones.add(_comunicacion);

                //mostrar en la consola, la informacion de la llamada.
                if(p_imprimirLote){
                    System.out.println(i+" "+_comunicacion.toString());
                }
            } 
        }
        
        return comunicaciones;
    }
    /**
     * Genera un numero telefonico al azar de la longitud pasada como parametro.
     * @param p_longitudNumero
     * @return 
     */
    public static String randomNro(int p_longitudNumero){
        String _numero = "";        
        //acotamos la generaciond e codigos de area el intervalo 51- 59 (America latina.)
        Integer _codigoArea = 5 + (int) (( Math.random()*8 )+1) ;
        for(int i=0; i<p_longitudNumero;i++){           
            _numero += (int) (Math.random()*9) ;
        }
        
        //se le pone + al 80% de los numeros 8 de cada 10
        return "+"+_codigoArea.toString() +_numero;
    }
    /**
     * Da un formato mas claro al tiempo para mejorar las referencias.
     * @param p_promedio valor promedio de la llamada.
     * @return 
     */
    public static String formatPromedio (double p_promedio){
        DecimalFormat f = new DecimalFormat("##.00");
        Double _value = p_promedio;
        String _unidad ="ms";
        if (_value>1000){
            _value = p_promedio / 1000;
            _unidad ="s";//segundos
            if (_value>60){
                _value = _value / 60;
                _unidad ="mins";   //minutos             
                if (_value>60){
                    _value = _value/60;
                    _unidad ="h";           //horas     
                    if (_value>24){
                        _value = _value/24.0;
                        _unidad ="d";              //  
                    }
                }
            }        
        }
         return f.format(_value) + _unidad;
    }
}
