/*
 *Ingenieria del software 2 
 *UNNE 2019
 */
package entidades;


import utiles.Herramienta;

public class Telefono {    
    private String nroTelefono;
    
//private String[] paises = new String[5];
    public Telefono (String p_nroTelefono){
        this.setNroTelefono(p_nroTelefono);
    }
    
    public String delPais(){
        //las llamadas locales no son de interes
        String _pais = "LOCAL";
        if (this.esInternacional()){             
            _pais = this.codigoDeArea();
        }
        return _pais;
    }
    /**
     * @return String que contiene los 6 primeros elementos del telefono.
     * ejemplo (+5411), +44(0), (44.171)9, +45 43,
     */
    public String codigoDeArea(){        
        String _pais="";
        String _nro  = this.soloNumeros();
        for (int i = 1; i<5;i++){
            _pais = Herramienta.codigos.get(Integer.parseInt(_nro.substring(0,i))) ;
            if (_pais!= null){i=100;}
        }        
        return _pais!=  null?_pais:"ERROR";
       
    }
    /**
     * remueve todos los caracteres basura del numero de telefono.
     * @return 
     */
    public String soloNumeros (){
        return this.getNroTelefono().replaceAll("[-'`~!@#$%^&*()_|+=?;:'\",.<>\\{\\}\\[\\]\\\\\\/ ]", "");
    }
    
    public boolean esInternacional(){                 
        return this.soloNumeros().length()>=11 || this.getNroTelefono().indexOf("+")!=-1;
    }

    public String getNroTelefono() {
        return nroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }
}


