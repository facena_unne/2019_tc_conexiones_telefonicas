/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analisis;

import entidades.Telefono;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;
import utiles.Herramienta;


/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Comunicacion {
    private Telefono origin;
    private Telefono destino;
    private Date inicio;// inicio de la comunicacion
    private Date fin;//fin de la comunicacion

    public Comunicacion(
        Telefono p_origen, Telefono p_destino, 
        Date p_inicio, Date p_fin )
    {
        this.setOrigin(p_origen);
        this.setDestino(p_destino);
        this.setInicio(p_inicio);
        this.setFin(p_fin);
    }
    public Comunicacion( Telefono p_destino )
    {
        this.setOrigin(p_destino);
        this.setDestino(p_destino);        
        int millisInDay = 60*60*1000;    
        Random random = new Random();        
        Long _timeMillis = System.currentTimeMillis();
        this.setInicio(new Timestamp(_timeMillis));
        this.setFin(new Timestamp(_timeMillis + random.nextInt(millisInDay)));
    }
    
    /**
     * Duracion en milisegundos segundos de una comunicacion
     * @return 
     */
    public long duracion (){        
        return this.getFin().getTime() - this.getInicio().getTime();
    }
    @Override
    public String toString(){
        return "Desde: " + this.getOrigin().getNroTelefono()
            +" a  " + this.getDestino().getNroTelefono()
            + " iniciada: "+ this.getInicio().toGMTString()
            + " terminada: "+ this.getFin().toGMTString()
            + " duracion: "+ Herramienta.formatPromedio(this.duracion());
    }
    public Telefono getOrigin() {
        return origin;
    }

    public void setOrigin(Telefono origin) {
        this.origin = origin;
    }

    public Telefono getDestino() {
        return destino;
    }

    public void setDestino(Telefono destino) {
        this.destino = destino;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }
    
    
}
