/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analisis;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import utiles.Herramienta;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class ejemploAnalisis {
    
    
    private static Map<String, Double> paisPromedio = new HashMap<String, Double>();
    
    public static void main(String [] args ) {
            
        Herramienta.readCodigosDeArea();

        //INICIAMOS LOTE DE PRUEBA ALEATORIO
        ArrayList<Comunicacion> comunicaciones = Herramienta.loteDePrueba(500,true);
        
     
        
        System.out.println("");
        System.out.println("------------------------------------------");
        System.out.println("------------------------------------------");
        System.out.println("------------------------------------------");
        System.out.println("Iniciamos Alanlisis");
        System.out.println("------------------------------------------");
        System.out.println("------------------------------------------");
        System.out.println("------------------------------------------");
        
        //Calculamos el promedio de llamadas al pais, desde cualquer origen.
        Iterator<Comunicacion> _iterador = comunicaciones.iterator();
        while(_iterador.hasNext()){
            Comunicacion _com  = _iterador.next();            
            if (paisPromedio.containsKey( _com.getDestino().delPais() ) ){
                double _promedio = paisPromedio.get(_com.getDestino().delPais());
                paisPromedio.replace(
                    _com.getDestino().delPais(),
                    (_promedio + _com.duracion() ) / 2
                );                
            }else{
                paisPromedio.put(_com.getDestino().delPais(), (double) _com.duracion()) ;
            }
                        
            //descomentar esta linas para ver el avanse del calculo de promedio.
            System.out.print(_com.getDestino().delPais() +" Numero destino: "+_com.getDestino().soloNumeros() +" Prom: ");            
            System.out.println(Herramienta.formatPromedio( paisPromedio.get( _com.getDestino().delPais())));
            
        }
        
        System.out.println("");
        System.out.println("------------------------------------------");
        System.out.println("Resultados Alanlisis");
        System.out.println("------------------------------------------");
          
        for(Map.Entry<String, Double> _entry : paisPromedio.entrySet()){
            System.out.println("Para: " + _entry.getKey() + ", promedio=" + Herramienta.formatPromedio( _entry.getValue() ));
        }
       
       

    }

}
